# Implementation of element-finder testing task for Agile Engine.

# Algorithm used

There is no complex heuristic due to limited time for implementation.

It tries to find an element with the same attribute as in the original file.

The priority of attributes is hardcoded in the source and could be tweaked if needed.

The CSS class will be tried to match in full.
Also, if there are several CSS class values, then all combinations will be tried.

The first successful search, which fill result the single only element,
will be displayed as a result. If "--all" was passed, then all other alternative
solutions will be displayed also (if any).

If needed, the source could be tweaked to change the debug level and display debug messages.

# Usage

    element-finder.pl [options] [orig_file] [diff_file] [id]

To see explanation for options, please run

    ./element-finder.pl -h

To see full POD, please run

    perldoc element-finder.pl

# Results

Comparison output for sample pages could be found in output.txt

