#!/usr/bin/env perl

use strict;
use warnings;

use Algorithm::Combinatorics qw/ combinations /;
use Getopt::Long;
use List::MoreUtils qw/ uniq /;
use Log::Any qw/ $log /;
use Log::Any::Adapter ( 'Stdout', log_level => 'debug' );
use Pod::Usage;
use Readonly;
use String::Util qw/ trim /;
use XML::LibXML;

=head1 NAME

element-finder.pl - Implementation of element-finder testing task for Agile Engine.

=head1 SYNOPSIS

element-finder.pl [options] [orig_file] [diff_file] [id]

=head1 OPTIONS

=over 8

=item B<-h>

Print a brief help message and exits.

Aliases: -help, -?

=item B<-o>

Mandatory.

File with original HTML/XML.

If not specified, then first positional parameter will be used.

Aliases: -orig, -orig_file

=item B<-d>

Mandatory.

File with different HTML/XML.

If not specified, then second positional parameter will be used.

Aliases: -diff, -diff_file

=item B<-i>

Optional, default value is 'make-everything-ok-button'.

The id of element to search in original file.

If not specified, then third positional parameter will be used.

If there is no third positional parameter, then default value will be used.

Aliases: -id

=item B<-a>

Optional, default is off.

Dump all found solutions, not just the best one.

Aliases: -all

=back

=head1 DESCRIPTION

Read and analyze the given input file, and finds a specific element,
even after changes, using a set of extracted attributes.

=head1 INTERNALS

=head2 LOGGING

To see the debug output, please change the source:

    use Log::Any::Adapter ( 'Stdout', log_level => 'info' );

To be:

    use Log::Any::Adapter ( 'Stdout', log_level => 'debug' );

=head2 SPEED AND MEMORY

The code is based on LibXML, which could be not best alternative for
real-world tasks, depending on requirements to memory, speed and need to validate XML.

Probably, PugiXML (L<https://github.com/yko/pugixml-perl>, L<https://pugixml.org/benchmark.html>)
could be better. However, LibXML and it's Perl modules was already installed on my machine,
and size of testing files is relatively small, so as for testing task I decided to use
what I have under hands.

=cut

#
# program arguments
#
my $orig_file;
my $diff_file;
my $orig_id = 'make-everything-ok-button';
my $all     = 0;
my $help    = 0;

#
# XML parsed data
#
my %ok_button_attrs = ();
my $ok_button_full_path;
my $ok_button_text;
my $ok_button_tag;

#
# Global vars and constants
#
my $parser = XML::LibXML->new();

Readonly my $XPATH_BY_ID   => '//*[@id="' . $orig_id . '"]';
Readonly my $XPATH_BY_ATTR => '//*[@%s="%s"]';
Readonly my $XPATH_ATTRS   => './@*';
Readonly my @ATTRS_PRIO    => qw/
    id
    title
    onclick
    class
    href
    rel
    /;

main();

=head2 INTERNAL METHODS

=over 4

=item B<main()>

The element-finder implementation.

B<Parameters>

    Absent.

B<Return value>

    Absent.

=back

=cut

sub main {
    parse_arguments();

    parse_orig_file();
    my $dom = $parser->parse_file($diff_file);

    # we need only unique, non-empty results returned by find_by_xpath()
    my @result = uniq grep {$_} (

        # at first, try to find by attributes
        # priorities of attributes are difined in @ATTRS_PRIO
        (
            map { find_by_attr( $dom, @$_ ) }       # search by attribute, value
            map { [ $_, $ok_button_attrs{$_} ] }    # attribute, value
            @ATTRS_PRIO                             # attribute
        ),

        # try to find element with the same text enclosed in the original element
        # note, this is not the same as "title" attribute of element
        find_by_xpath( $dom, '//*[text()="' . $ok_button_text . '"]' ),

        # try to find element by exactly same xpath as it was in the original element
        # note, this could be element with completely different attributes and text
        find_by_xpath( $dom, $ok_button_full_path ),

        # as last resort, if nothing else helped, try to find the same element,
        # and don't take into account nor attributes, nor text, nor full xpath
        # the chance to have exactly one such tag in target document is near to zero
        # but anyway, this is better than nothing
        find_by_xpath( $dom, "//$ok_button_tag" ),
    );

    if (@result) {
        print shift(@result), "\n";    # show the best solution

        if ($all) {
            if (@result) {
                $log->info( 'Found ' . scalar(@result) . ' alternative solution(s):' );
                print join( "\n" => @result, '' );    # add extra new-line
            }
            else {
                $log->debug('The only one solution found');
            }
        }
    }
    else {
        print "Element not found\n";
    }
}

=over 4

=item B<parse_arguments()>

Parse command-line arguments.

Extra arguments (which are not options) are treated as original file,
different (target) file, and ID of element in original file.
All extra arguments have precendence over values passed as options.

B<Parameters>

    Absent.

B<Return value>

    Absent.

=back

=cut

sub parse_arguments {
    GetOptions(
        "o|orig|orig_file=s" => \$orig_file,
        "d|diff|diff_file=s" => \$diff_file,
        "i|id=s"             => \$orig_id,
        "a|all"              => \$all,
        'h|help|?'           => \$help,
    );

    pod2usage(1) if $help;

    if (@ARGV) {
        $orig_file = shift(@ARGV);
    }
    if (@ARGV) {
        $diff_file = shift(@ARGV);
    }
    if (@ARGV) {
        $orig_id = shift(@ARGV);
    }

    $log->debug( 'Orig file: ' . $orig_file );
    $log->debug( 'Diff file: ' . $diff_file );
    $log->debug( 'ID to search: ' . $orig_id );
}

=over 4

=item B<parse_orig_file()>

Parse original file.
Store found information in global variables.

B<Parameters>

    Absent.

B<Return value>

    Absent.

=back

=cut

sub parse_orig_file {
    my $dom = $parser->parse_file($orig_file);

    my @elements = $dom->findnodes($XPATH_BY_ID);

    if ( @elements == 0 ) {
        print STDERR "Can't find element by specified id, aborting\n";
        exit(1);
    }

    # this should never happen for well-formed XML, but just to be safe
    if ( @elements > 1 ) {
        print STDERR "There is more than one element by specified id, aborting\n";
        exit(1);
    }

    my $ok_button = $elements[0];
    $ok_button_tag = $ok_button->nodeName();

    my @children               = $ok_button->childNodes;
    my $ok_button_text_element = $children[0];
    $ok_button_text = trim($ok_button_text_element);

    my @attrs = $ok_button->findnodes($XPATH_ATTRS);
    for my $attr (@attrs) {
        $ok_button_attrs{ $attr->nodeName } = $attr->value;
    }

    $ok_button_full_path = $ok_button->nodePath();
}

=over 4

=item B<find_by_attr($dom, $attr_name, $attr_val)>

Try to find element by given attribute name and value.

B<Parameters>

$dom

    Parsed DOM to search in.

$attr_name

    Attribute name to search.

$attr_val

    Attribute value to search.

    For attribute "class" there is special handling: if value consist of
    several values, then also try to find by each class combinations.

B<Return value>

    Return list of all found xpath variants.

=back

=cut

sub find_by_attr {
    my ( $dom, $attr_name, $attr_val ) = @_;
    my @result;

    my $xpath  = sprintf( $XPATH_BY_ATTR, $attr_name, $attr_val );
    push( @result, find_by_xpath( $dom, $xpath ) );

    if ( $attr_name eq 'class' ) {
        my @classes     = split( /\s+/, $attr_val );
        my $classes_cnt = @classes;
        if ( $classes_cnt > 1 ) {
            $log->debug( 'Searching by class combinations' );
            for my $i ( reverse( 1 .. $classes_cnt - 1 ) ) {
                my $iter = combinations( \@classes, $i );
                while ( my $combination = $iter->next ) {
                    my $class = join( ' ' => @$combination );
                    $xpath = sprintf( $XPATH_BY_ATTR, $attr_name, $class );
                    push( @result, find_by_xpath( $dom, $xpath ) );
                }
            }
        }
    }

    return @result;
}

=over 4

=item B<find_by_xpath($dom, $xpath)>

Try to find element by given xpath.

B<Parameters>

$dom

    Parsed DOM to search in.

$xpath

    XPath to search by.

B<Return value>

    If found exactly one element, then return it's xpath.

    Otherwise, just return empty result.

=back

=cut

sub find_by_xpath {
    my ( $dom, $xpath ) = @_;

    $log->debug( 'Searching by ' . $xpath );
    my @elements = $dom->findnodes($xpath);
    if ( @elements == 0 ) {
        $log->debug('Nothing found');
    }
    elsif ( @elements > 1 ) {
        $log->debug('Found more than one element for given criteria, ignoring');
    }
    else {
        $log->debug( 'Found exactly one element: ', $elements[0]->nodePath() );
        return $elements[0]->nodePath();
    }

    # in case of unsuccessful search, return nothing
    return;
}

